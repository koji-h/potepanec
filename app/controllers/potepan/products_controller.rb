class Potepan::ProductsController < ApplicationController
  MAX_COUNT_RELATED_PRODUCT = 4

  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.
      includes(master: [:default_price, :images]).
      limit(MAX_COUNT_RELATED_PRODUCT)
  end
end

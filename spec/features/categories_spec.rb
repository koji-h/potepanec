require 'rails_helper'

RSpec.describe "Category", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product) }

  before do
    visit potepan_category_path(taxon.id)
  end

  scenario "display correct category name" do
    within("div.sideBar") do
      expect(page).to have_content taxonomy.name
    end
    within("div.page-title") do
      expect(page).to have_content taxon.name
    end
  end

  scenario "display product name" do
    within("div.productBox") do
      expect(page).to have_content product.display_price
      expect(page).to have_content product.name
      expect(page).not_to have_content other_product.name
    end
  end

  scenario "link to product detail page" do
    click_on taxon.name
    expect(page).to have_content product.name
    expect(page).not_to have_content other_product.name
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  scenario "link to 一覧ページへ戻る" do
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario "display sidebar" do
    within("div.sideBar") do
      click_on taxonomy.name
      expect(page).to have_content taxon.name
      click_on taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end
end

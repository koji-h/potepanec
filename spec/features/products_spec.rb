require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:other_related_product) { create(:product) }

  background do
    visit potepan_product_path(product.id)
  end

  scenario "access product" do
    expect(page).to have_title product.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
  end

  scenario "transition to the top page" do
    click_link "HOME", match: :prefer_exact
    expect(current_path).to eq potepan_path
  end

  scenario "product name and price of related products are displayed" do
    within("div.productBox") do
      expect(page).to have_content related_product.name
      expect(page).to have_content related_product.display_price
      expect(page).not_to have_content product.name
    end
  end

  scenario "unrelated product are not displayed" do
    expect(page).not_to have_content other_related_product.id
  end

  scenario "click related product to move to product details page" do
    click_on related_product.name
    expect(page).to have_current_path potepan_product_path related_product.id
  end
end

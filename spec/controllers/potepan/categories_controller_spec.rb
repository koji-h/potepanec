require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "GET#show" do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:products) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it "returns a 200 response" do
      expect(response).to have_http_status "200"
    end

    it "responds render_template" do
      expect(response).to render_template :show
    end

    it "taxon have expect object" do
      expect(assigns(:taxon)).to match(taxon)
    end

    it "product have expect object" do
      expect(assigns(:products)).to match_array(products)
    end
  end
end

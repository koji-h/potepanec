require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "GET#show" do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product) }

    before do
      get :show, params: { id: product.id }
    end

    it "returns a 200 response" do
      expect(response).to have_http_status "200"
    end

    it "assigns @product" do
      expect(assigns(:product)).to eq product
    end

    it "responds render_template" do
      expect(response).to render_template :show
    end

    context "when there are 3 related products" do
      let!(:related_products) { create_list(:product, 3, taxons: [taxon]) }

      it "3 related products are displayed" do
        expect(assigns(:related_products)).to eq related_products.first(3)
      end
    end

    context "when there are 4 related products" do
      let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

      it "4 related products are displayed" do
        expect(assigns(:related_products)).to eq related_products.first(4)
      end
    end

    context "when there are 5 related products" do
      let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

      it "4 related products are displayed" do
        expect(assigns(:related_products)).to eq related_products.first(4)
      end
    end
  end
end

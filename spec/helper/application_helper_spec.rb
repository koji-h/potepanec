require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  context "if there is page title" do
    it "full_title is displayed" do
      expect(full_title("title")).to eq "title - BIGBAG Store"
    end
  end

  context "if there is not page title" do
    it "return BIGBAG store if blank" do
      expect(full_title("")).to eq "BIGBAG Store"
    end
  end

  context "nil to page title" do
    it "return BIGBAG store" do
      expect(full_title(nil)).to eq "BIGBAG Store"
    end
  end

  context "no arguments page title" do
    it "return BIGBAG store" do
      expect(full_title).to eq "BIGBAG Store"
    end
  end
end
